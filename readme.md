# Introducción
El siguiete repositorio contiene los contenedores de docker que se manejan en el proyecto de horarios.
Para revisar los contenedores disponibles puedes revisar el archivo 'docker-compose.yml'. Este contiente:
### Backend en Ruby on Rails:
Se ejecuta en el puerto 3000
### Frontend en React
Se ejecuta en el puerto 3001
### Postgres 15.
Se ejecuta en el puerto 5432
### PgAdmin 4.
Se ejecuta en el puerto 8080


# Construir los contenedores de docker
Para construir los contenedores de docker se debe ejecutar en terminal

```docker-compose build```

# Arrancar contenedores de docker
Para arrancar los contenedores de docker se debe ejecutar en terminal

```docker-compose up```